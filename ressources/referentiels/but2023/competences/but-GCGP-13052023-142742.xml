<?xml version="1.0" encoding="UTF-8"?>
<referentiel_competence specialite="GCGP"
                        specialite_long="Génie Chimique - Génie des Procédés"
                        type="B.U.T." annexe="6"
                        type_structure="type1"
                        type_departement="secondaire"
                        version="2022-03-31 00:00:00"
>
    <competences>
                <competence nom_court="Production"
                    numero="1"
                    libelle_long="Produire avec une installation industrielle de transformation de la matière et de l’énergie par voie chimique ou biologique"
                    couleur="c1"
                    id="d4930e39e9ffd61e21d75ecb107909a6">
            <situations>
                                <situation>Préparation, planification, mise en route et arrêt d&#039;une production</situation>
                                <situation>Suivi de production</situation>
                                <situation>Après la production : analyse de la production et communication d&#039;un rapport </situation>
                                <situation>Analyse des dysfonctionnements et prévision des opérations de maintenance </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En respectant les modes opératoires et les procédures de fabrication</composante>
                                <composante>En assurant la sécurité des biens et des personnes</composante>
                                <composante>En tenant compte des contraintes de production (délais, spécifications produits, disponibilités matières premières, moyens matériels et humains)</composante>
                                <composante>En atteignant les objectifs de qualité de produit et de quantité de production</composante>
                                <composante>En réduisant au maximum l’impact environnemental et sanitaire de l’activité </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Conduire un équipement de production dans une posture d&#039;opérateur" annee="BUT1">
                <acs>
                                        <ac code="AC11.01">Démarrer un équipement et vérifier son bon fonctionnement</ac>
                                        <ac code="AC11.02">Faire fonctionner un équipement en fonctionnement normal et l&#039;arrêter</ac>
                                        <ac code="AC11.03">Faire des relevés de grandeurs physico-chimiques sur un équipement ou une installation </ac>
                                        <ac code="AC11.04">Détecter des écarts par rapport au régime nominal</ac>
                                        <ac code="AC11.05">Compléter un document de suivi de production</ac>
                                        <ac code="AC11.06">Communiquer au sein d&#039;une équipe de production</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Assurer le bon fonctionnement d&#039;une installation dans une posture de technicien" annee="BUT2">
                <acs>
                                        <ac code="AC21.01">Assurer le suivi de production et agir sur les paramètres pour rester au régime nominal</ac>
                                        <ac code="AC21.02">Effectuer des mesures et des calculs pour analyser le fonctionnement et évaluer les performances de l&#039;installation </ac>
                                        <ac code="AC21.03">Rédiger un rapport de suivi de la production et communiquer les résultats</ac>
                                        <ac code="AC21.04">Détecter une anomalie, alerter, solliciter les actions de maintenance </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Gérer une unité de production dans une posture de chef d&#039;équipe " annee="BUT3">
                <acs>
                                        <ac code="AC31.01">Vérifier au préalable la conformité et le bon fonctionnement d&#039;un équipement </ac>
                                        <ac code="AC31.02">Prévoir et opérer l&#039;adaptation du procédé aux changements de production</ac>
                                        <ac code="AC31.03">Animer une équipe en situation de production</ac>
                                        <ac code="AC31.04">Participer à l&#039;élaboration de consignes, de protocoles</ac>
                                        <ac code="AC31.05">Préparer et organiser la mise à disposition des installations</ac>
                                        <ac code="AC31.06">Suggérer et proposer des améliorations ou investissements pour des installations de son périmètre </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Dimensionner"
                    numero="2"
                    libelle_long="Concevoir des équipements de production industrielle de transformation de la matière et de l’énergie par voie chimique ou biologique"
                    couleur="c2"
                    id="b1f71a20cce65ab1034d84f1b59c46a9">
            <situations>
                                <situation>Dimensionnement d&#039;équipements</situation>
                                <situation>Aide au choix d&#039;équipements</situation>
                                <situation>Réalisation de schémas techniques</situation>
                                <situation>Transformation et conception d&#039;unités de production</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En respectant un cahier des charges</composante>
                                <composante>En respectant les règles de sécurité et de protection de l&#039;environnement</composante>
                                <composante>En s&#039;appuyant sur de la documentation technique adéquate</composante>
                                <composante>En s&#039;assurant de la fiabilité des propositions techniques élaborées</composante>
                                <composante>En réduisant la consommation des ressources et la consommation énergétique </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Choisir des appareils et dimensionner des réseaux d&#039;utilité" annee="BUT1">
                <acs>
                                        <ac code="AC12.01">Choisir des appareils de transport de fluide auprès d’équipementiers (pompes, compresseur, pompe à vide, éléments de robinetterie...) </ac>
                                        <ac code="AC12.02">Choisir des capteurs et des actionneurs adaptés aux conditions opératoires</ac>
                                        <ac code="AC12.03">Dimensionner des réseaux hydrauliques</ac>
                                        <ac code="AC12.04">Identifier le besoin et dimensionner un réseau d’utilité (vide, air comprimé, refroidissement, chauffage...)</ac>
                                        <ac code="AC12.05">Lire, interpréter et réaliser des schémas techniques simples (réalisation sur existant) </ac>
                                        <ac code="AC12.06">Rendre compte et échanger au sein d&#039;une équipe projet</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Prédimensionner des appareillages de réaction et de séparation" annee="BUT2">
                <acs>
                                        <ac code="AC22.01">Lire, interpréter et réaliser des schémas techniques d&#039;équipements et d&#039;installations complexes</ac>
                                        <ac code="AC22.02">Rédiger un cahier des charges d’équipement</ac>
                                        <ac code="AC22.03">Prédimensionner des appareils de réaction (chimique et/ou biologique)</ac>
                                        <ac code="AC22.04">Prédimensionner des appareils de séparation</ac>
                                        <ac code="AC22.05">Dimensionner des équipements thermiques et énergétiques (calorifugeage, échangeur, chaudière...)</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="3" libelle="Prédimensionner des appareillages complexes et faire des choix technologiques " annee="BUT3">
                <acs>
                                        <ac code="AC32.01">Proposer des solutions techniques pour répondre à un besoin (procédé de réaction ou séparation) </ac>
                                        <ac code="AC32.02">Dimensionner des appareils de séparation</ac>
                                        <ac code="AC32.03">Analyser le procédé et définir les régulations à mettre en place</ac>
                                        <ac code="AC32.04">Lire, interpréter et réaliser le PID d&#039;une installation</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Contrôler"
                    numero="3"
                    libelle_long="Contrôler la qualité des matières premières et des produits "
                    couleur="c3"
                    id="1dc2ae33cf098737385a57be1bbbca56">
            <situations>
                                <situation>Analyse physico-chimique ou microbiologique d&#039;un produit</situation>
                                <situation>Détection d&#039;une impureté</situation>
                                <situation>Contrôle des paramètres de production </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En respectant les bonnes pratiques de laboratoire</composante>
                                <composante>En maintenant une traçabilité des résultats</composante>
                                <composante>En choisissant un protocole adapté</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Analyser des produits par des méthodes simples - Laborantin" annee="BUT1">
                <acs>
                                        <ac code="AC13.01">Préparer des solutions et des échantillons</ac>
                                        <ac code="AC13.02">Mesurer les caractéristiques physico-chimiques des produits</ac>
                                        <ac code="AC13.03">Réaliser des dosages volumétriques en laboratoire</ac>
                                        <ac code="AC13.04">Consigner les résultats dans un cahier de laboratoire </ac>
                                        <ac code="AC13.05">Caractériser et suivre les transformations physiques, chimiques et biologiques</ac>
                                        <ac code="AC13.06">Mettre en forme et rendre compte de résultats</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Identifier et quantifier des constituants par des méthodes adaptées - Technicien de laboratoire " annee="BUT2">
                <acs>
                                        <ac code="AC23.01">Mettre en œuvre les techniques classiques d&#039;analyse</ac>
                                        <ac code="AC23.02">Interpréter les résultats et la conformité des produits</ac>
                                        <ac code="AC23.03">Rédiger et communiquer les comptes rendus d&#039;analyse</ac>
                                        <ac code="AC23.04">Enregistrer les comptes rendus et assurer leur tracabilité</ac>
                                        <ac code="AC23.05">Evaluer les risques liés aux procédés et à la manipulation des matières premières et produits.</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Développement et amélioration"
                    numero="4"
                    libelle_long="Développer et améliorer des procédés de transformation de la matière et de l&#039;énergie par voie chimique ou biologique "
                    couleur="c4"
                    id="421f773ee403b8d608bcc08c75844f1c">
            <situations>
                                <situation>Mise au point et amélioration de procédés</situation>
                                <situation>Industrialisation de procédés</situation>
                                <situation>Campagnes d&#039;essais pilotes</situation>
                                <situation>Détermination de performances d&#039;équipements</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En mettant en place une démarche expérimentale</composante>
                                <composante>En respectant les règles relatives à l&#039;hygiène, la sécurité et à la protection de l’environnement</composante>
                                <composante>En assurant une veille des innovations technologiques</composante>
                                <composante>En s’inscrivant dans une démarche de développement durable (optimisation énergétique, réduction de l&#039;impact environnemental, valorisation des déchets..) </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Réaliser et interpréter des essais et tests sur des équipements pilotes " annee="BUT2">
                <acs>
                                        <ac code="AC24.01CPIT">Réaliser des tests et essais de mise au point de procédés</ac>
                                        <ac code="AC24.02CPIT">Relever les données et les consigner dans les cahiers de laboratoire et bases de données </ac>
                                        <ac code="AC24.03CPIT">Analyser les résultats des tests et faire des propositions</ac>
                                        <ac code="AC24.04CPIT">Prédire l’influence des paramètres de fonctionnement </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Utiliser des outils de modélisation et mettre en place des essais afin d&#039;optimiser et développer des procédés " annee="BUT3">
                <acs>
                                        <ac code="AC34.01CPIT">Modéliser une opération (calcul, simulation, modélisation) </ac>
                                        <ac code="AC34.02CPIT">Optimiser un procédé</ac>
                                        <ac code="AC34.03CPIT">Mettre en place des campagnes d’essais</ac>
                                        <ac code="AC34.04CPIT">Participer à l&#039;industrialisation des procédés </ac>
                                        <ac code="AC34.05CPIT">Participer au développement des procédés innovants </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Protéger"
                    numero="4"
                    libelle_long="Réduire l&#039;impact environnemental d&#039;une activité industrielle ou urbaine "
                    couleur="c4"
                    id="7e89a0685f6ea485d5ad1c9e34173100">
            <situations>
                                <situation>Conduite d&#039;une unité de traitement des rejets</situation>
                                <situation>Etudes techniques en R&amp;D</situation>
                                <situation>Suivi en Service Utilités</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En respectant les bonnes pratiques de fabrication</composante>
                                <composante>En identifiant les paramètres pertinents sur l&#039;installation</composante>
                                <composante>En favorisant les procédés propres et sûrs</composante>
                                <composante>En contribuant à la sécurité des procédés</composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Identifier les rejets et les solutions techniques de traitement (dans la posture d&#039;un technicien supérieur)" annee="BUT2">
                <acs>
                                        <ac code="AC24.01CQESP">Identifier les principaux textes réglementaires et relatifs aux pollutions et aux nuisances applicables au contexte </ac>
                                        <ac code="AC24.02CQESP">Faire fonctionner une installation de traitement des rejets</ac>
                                        <ac code="AC24.03CQESP">Repérer les points pertinents de prélèvement et assurer le suivi des prélèvements</ac>
                                        <ac code="AC24.04CQESP">Identifier les différentes filières d&#039;élimination et de valorisation des déchets</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Proposer une démarche de réduction de l&#039;impact des rejets (dans la posture d&#039;un assistant ingénieur) " annee="BUT3">
                <acs>
                                        <ac code="AC34.01CQESP">Réaliser un diagnostic pollution</ac>
                                        <ac code="AC34.02CQESP">Mettre en place un plan de suivi des déchets</ac>
                                        <ac code="AC34.03CQESP">Proposer des solutions techniques de traitements et valorisation des effluents et de valorisation des rejets ou coproduits</ac>
                                        <ac code="AC34.04CQESP">Initier des actions de réduction de l&#039;impact environnemental d&#039;une activité industrielle (optimisation énergétique, collaboration inter-sites, ACV, économie circulaire...)</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Piloter"
                    numero="4"
                    libelle_long="Piloter une installation automatisée de transformation de la matière et de l’énergie par voie chimique ou biologique"
                    couleur="c4"
                    id="839e14668399f4d7e73dbc83c99a7a0b">
            <situations>
                                <situation>Mise en route et arrêt d&#039;une production automatisée</situation>
                                <situation>Fonctionnement d&#039;une installation en interagissant avec l&#039;automatisme </situation>
                                <situation>Gestion d&#039;incidents, de dérives, d&#039;anomalies, de dysfonctionnements </situation>
                                <situation>Aide à la décision concernant le choix de matériel </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En réagissant de façon adaptée aux aléas pour assurer la continuité de la production</composante>
                                <composante>En communiquant de manière efficace avec les équipes de production et les services support </composante>
                                <composante>En assurant la sécurité des personnes et des outils de production </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Piloter une installation automatisée dans une posture de technicien " annee="BUT2">
                <acs>
                                        <ac code="AC24.01CPOP">Démarrer, faire fonctionner et arrêter une installation automatisée simple</ac>
                                        <ac code="AC24.02CPOP">Gérer un redémarrage après un arrêt d&#039;urgence</ac>
                                        <ac code="AC24.03CPOP">Utiliser les différentes fonctionnalités des outils de supervision en fonctionnement normal ou dégradé </ac>
                                        <ac code="AC24.04CPOP">Identifier les grandeurs caractéristiques d&#039;une boucle de régulation simple et en régler les paramètres </ac>
                                        <ac code="AC24.05CPOP">Proposer des matériels nécessaires à la conduite d&#039;une installation simple </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Gérer la production et conduire des actions d&#039;amélioration d&#039;une installation automatisée " annee="BUT3">
                <acs>
                                        <ac code="AC34.01CPOP">Démarrer, faire fonctionner et arrêter une installation automatisée complexe</ac>
                                        <ac code="AC34.02CPOP">Analyser le fonctionnement d&#039;une installation automatisée complexe dans le but de l&#039;optimiser</ac>
                                        <ac code="AC34.03CPOP">Proposer et tester des structures et des paramètres de régulation d&#039;une ou plusieurs grandeurs sur une installation </ac>
                                        <ac code="AC34.04CPOP">Participer à l&#039;amélioration d&#039;un système de supervision </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Gestion de projets industriels"
                    numero="5"
                    libelle_long="Participer à la définition et au suivi d&#039;un projet d&#039;installation d&#039; équipements de transformation de la matière et de l&#039;énergie par voie chimique ou biologique"
                    couleur="c5"
                    id="81cda620e37d0d4e05da7b8756a6c07a">
            <situations>
                                <situation>Participation au montage d&#039;un projet</situation>
                                <situation>Réalisation d&#039;études techniques en réponse à un cahier des charges</situation>
                                <situation>Management d&#039;activités au sein d&#039;un projet</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En tenant compte des contraintes du projet industriel (délais et moyens matériels, humains et financiers)</composante>
                                <composante>En tenant compte des contextes réglementaires et environnementaux</composante>
                                <composante>En s&#039;assurant de la fiabilité des propositions techniques élaborées</composante>
                                <composante>En réduisant la consommation des ressources et la consommation énergétique </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Réaliser des activités au sein d&#039;un projet industriel" annee="BUT2">
                <acs>
                                        <ac code="AC25.01CPIT">Identifier les différents acteurs d&#039;un projet industriel</ac>
                                        <ac code="AC25.02CPIT">S&#039;intégrer dans une équipe projet</ac>
                                        <ac code="AC25.03CPIT">Concevoir des dossiers techniques pour des équipements thermiques et énergétiques</ac>
                                        <ac code="AC25.04CPIT">Participer au dimensionnement d&#039;une unité industrielle</ac>
                                        <ac code="AC25.05CPIT">Concevoir des descriptifs et des procédures d’utilisation de nouvelles installations</ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="̂Etre en responsabilité de l&#039;exécution d&#039;activités au sein d&#039;un projet" annee="BUT3">
                <acs>
                                        <ac code="AC35.01CPIT">Participer à la définition, planification et suivi d&#039;un projet industriel</ac>
                                        <ac code="AC35.02CPIT">Manager au sein d&#039;une équipe projet et interagir avec différents acteurs</ac>
                                        <ac code="AC35.03CPIT">Participer à l&#039;évaluation des coûts de l’installation et du fonctionnement</ac>
                                        <ac code="AC35.04CPIT">Concevoir des dossiers techniques pour des appareils de réaction et de séparation</ac>
                                        <ac code="AC35.05CPIT">Suivre l’installation d&#039;équipements de production et assurer la formation des utilisateurs </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Gérer"
                    numero="5"
                    libelle_long="Garantir la sécurité et la conformité des procédés et des produits"
                    couleur="c5"
                    id="8a04407295cc3b84e4396799a4a4b0c7">
            <situations>
                                <situation>Evaluation et gestion des risques en unité de production</situation>
                                <situation>Conduite d&#039;audits internes</situation>
                                <situation>Etudes en Recherche et développement</situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En respectant les bonnes pratiques de fabrication</composante>
                                <composante>En mettant en œuvre une démarche participative d’amélioration continue de la qualité et de la sécurité dans l’acte de production</composante>
                                <composante>En contribuant à la sécurité des procédés </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Suivre la conformité et la sécurité des installations et des produits " annee="BUT2">
                <acs>
                                        <ac code="AC25.01CQESP">Identifier les risques et participer à la mise en œuvre d&#039;une méthode d&#039;évaluation des risques </ac>
                                        <ac code="AC25.02CQESP">Comprendre les principaux textes réglementaires et principales normes</ac>
                                        <ac code="AC25.03CQESP">Appliquer les règles et procédures</ac>
                                        <ac code="AC25.04CQESP">Assurer le suivi réglementaire des installations et des produits </ac>
                                        <ac code="AC25.05CQESP">Participer à l&#039;élaboration et à la mise à jour de documents du système de management de la qualité et de la sécurité </ac>
                                        <ac code="AC25.06CQESP">Communiquer en interne et en externe sur les aspects réglementaires et normatifs </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Proposer des actions dans une démarche d&#039;amélioration continue " annee="BUT3">
                <acs>
                                        <ac code="AC35.01CQESP">Identifier et analyser les exigences des réglementations et normes</ac>
                                        <ac code="AC35.02CQESP">Réaliser une étude de risques</ac>
                                        <ac code="AC35.03CQESP">Mettre au point et valider des méthodes d’analyse des produits et les protocoles associés </ac>
                                        <ac code="AC35.04CQESP">Interpréter et exploiter les résultats d&#039;analyse</ac>
                                        <ac code="AC35.05CQESP">Mettre en place un plan d’action suite à la détection de non confomité</ac>
                                        <ac code="AC35.06CQESP">Animer une démarche d&#039;amélioration continue </ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
                <competence nom_court="Optimiser"
                    numero="5"
                    libelle_long="Optimiser la production d&#039;une installation industrielle de transformation de la matière et de l’énergie par voie chimique ou biologique "
                    couleur="c5"
                    id="d11d45c2de55fd88d49d8cc9273791bf">
            <situations>
                                <situation>Planification d&#039;une production (côté ordonnancement, méthodes) </situation>
                                <situation>Optimisation des réglages et des paramètres de fonctionnement</situation>
                                <situation>Analyse des dysfonctionnements dans un but d&#039;optimisation</situation>
                                <situation>Après la production : analyse de la production et communication d&#039;un rapport </situation>
                            </situations>
            <composantes_essentielles>
                                <composante>En tenant compte des contraintes de production (délais, spécifications produits, disponibilités matières premières, moyens matériels et humains)</composante>
                                <composante>En tenant compte des contraintes environnementales</composante>
                                <composante>En améliorant la sécurité des personnes et des biens </composante>
                            </composantes_essentielles>
            <niveaux>
                                <niveau ordre="1" libelle="Identifier les paramètres critiques d&#039;une production, analyser les déviations " annee="BUT2">
                <acs>
                                        <ac code="AC25.01CPOP">Analyser des variations et anomalies par rapport aux spécifications de production</ac>
                                        <ac code="AC25.02CPOP">Produire et communiquer des rapports d&#039;analyse</ac>
                                        <ac code="AC25.03CPOP">Évaluer les coûts de production</ac>
                                        <ac code="AC25.04CPOP">Déterminer les paramètres influençant la production (qualité des produits, quantité de production) </ac>
                                    </acs>
                </niveau>
                                <niveau ordre="2" libelle="Optimiser la production en tant que chef d&#039;équipe ou assistant ingénieur " annee="BUT3">
                <acs>
                                        <ac code="AC35.01CPOP">Organiser la production</ac>
                                        <ac code="AC35.02CPOP">Proposer des solutions de réduction des coûts de production</ac>
                                        <ac code="AC35.03CPOP">Déterminer les paramètres permettant de réduire l&#039;impact environnemental du procédé</ac>
                                        <ac code="AC35.04CPOP">Ajuster les paramètres permettant d&#039;optimiser la production</ac>
                                    </acs>
                </niveau>
                            </niveaux>
        </competence>
            </competences>
    <parcours>
                <parcour numero="0" libelle="Conception des Procédés et Innovation Technologique" code="CPIT">
                        <annee ordre="1">
                                <competence niveau="1" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="1" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="1" id="1dc2ae33cf098737385a57be1bbbca56"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="2" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="2" id="1dc2ae33cf098737385a57be1bbbca56"/>
                                <competence niveau="1" id="421f773ee403b8d608bcc08c75844f1c"/>
                                <competence niveau="1" id="81cda620e37d0d4e05da7b8756a6c07a"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="3" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="2" id="421f773ee403b8d608bcc08c75844f1c"/>
                                <competence niveau="2" id="81cda620e37d0d4e05da7b8756a6c07a"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Contrôle, Qualité, Environnement et Sécurité des Procédés" code="CQESP">
                        <annee ordre="1">
                                <competence niveau="1" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="1" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="1" id="1dc2ae33cf098737385a57be1bbbca56"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="2" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="2" id="1dc2ae33cf098737385a57be1bbbca56"/>
                                <competence niveau="1" id="7e89a0685f6ea485d5ad1c9e34173100"/>
                                <competence niveau="1" id="8a04407295cc3b84e4396799a4a4b0c7"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="3" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="2" id="7e89a0685f6ea485d5ad1c9e34173100"/>
                                <competence niveau="2" id="8a04407295cc3b84e4396799a4a4b0c7"/>
                            </annee>
                    </parcour>
                <parcour numero="0" libelle="Contrôle, Pilotage et Optimisation des Procédés" code="CPOP">
                        <annee ordre="1">
                                <competence niveau="1" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="1" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="1" id="1dc2ae33cf098737385a57be1bbbca56"/>
                            </annee>
                        <annee ordre="2">
                                <competence niveau="2" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="2" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="2" id="1dc2ae33cf098737385a57be1bbbca56"/>
                                <competence niveau="1" id="839e14668399f4d7e73dbc83c99a7a0b"/>
                                <competence niveau="1" id="d11d45c2de55fd88d49d8cc9273791bf"/>
                            </annee>
                        <annee ordre="3">
                                <competence niveau="3" id="d4930e39e9ffd61e21d75ecb107909a6"/>
                                <competence niveau="3" id="b1f71a20cce65ab1034d84f1b59c46a9"/>
                                <competence niveau="2" id="839e14668399f4d7e73dbc83c99a7a0b"/>
                                <competence niveau="2" id="d11d45c2de55fd88d49d8cc9273791bf"/>
                            </annee>
                    </parcour>
            </parcours>
</referentiel_competence>
