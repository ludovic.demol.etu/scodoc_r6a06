"""long password_hash

Revision ID: b4859c04205f
Revises: c8f66652c77f
Create Date: 2023-12-06 03:23:33.992635

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "b4859c04205f"
down_revision = "c8f66652c77f"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.alter_column(
            "password_hash",
            existing_type=sa.VARCHAR(length=128),
            type_=sa.Text(),
            existing_nullable=True,
        )


def downgrade():
    with op.batch_alter_table("user", schema=None) as batch_op:
        batch_op.alter_column(
            "password_hash",
            existing_type=sa.Text(),
            type_=sa.VARCHAR(length=128),
            existing_nullable=True,
        )
