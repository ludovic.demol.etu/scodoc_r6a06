# -*- mode: python -*-
# -*- coding: utf-8 -*-

##############################################################################
#
# Gestion scolarite IUT
#
# Copyright (c) 1999 - 2024 Emmanuel Viennet.  All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Emmanuel Viennet      emmanuel.viennet@viennet.net
#
##############################################################################


""" Verification version logiciel vs version "stable" sur serveur
    N'effectue pas la mise à jour automatiquement, mais permet un affichage d'avertissement.
"""
import json
import requests
import time
from flask import current_app

from app import log
import app.scodoc.sco_utils as scu
from sco_version import SCOVERSION, SCONAME


def is_up_to_date() -> str:
    """Check installed version vs last release.
    Returns html message, empty of ok.
    """
    if current_app.testing or current_app.debug:
        return "<div>Mode développement</div>"
    diag = ""
    try:
        response = requests.get(
            scu.SCO_UP2DATE + "/" + SCOVERSION, timeout=scu.SCO_ORG_TIMEOUT
        )
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
        current_app.logger.debug("is_up_to_date: %s", diag)
        return f"""<div>Attention: installation de {SCONAME} non fonctionnelle.</div>
        <div>Détails: pas de connexion à {scu.SCO_WEBSITE}.
        Vérifier paramètrages réseau,
        <a href="https://scodoc.org/GuideInstallDebian11/#informations-sur-les-flux-reseau">voir la documentation</a>.
        </div>
        """
    except:
        current_app.logger.debug("is_up_to_date: %s", diag)
        return f"""<div>Attention: installation de {SCONAME} non fonctionnelle.</div>
        <div>Détails: erreur inconnue lors de la connexion à {scu.SCO_WEBSITE}.
        Vérifier paramètrages réseau,
        <a href="https://scodoc.org/GuideInstallDebian11/#informations-sur-les-flux-reseau">voir la documentation</a>.
        </div>
        """
    if response.status_code != 200:
        current_app.logger.debug(
            f"is_up_to_date: invalid response code ({response.status_code})"
        )
        return f"""<div>Attention: réponse invalide de {scu.SCO_WEBSITE}</div>
        <div>(erreur http {response.status_code}).</div>"""

    try:
        infos = json.loads(response.text)
    except json.decoder.JSONDecodeError:
        current_app.logger.debug(f"is_up_to_date: invalid response (json)")
        return f"""<div>Attention: réponse invalide de {scu.SCO_WEBSITE}</div>
        <div>(erreur json).</div>"""

    if infos["status"] != "ok":
        # problème coté serveur, ignore discrètement
        log(f"is_up_to_date: server {infos['status']}")
        return ""
    if (SCOVERSION != infos["last_version"]) and (
        (time.time() - infos["last_version_date"]) > (24 * 60 * 60)
    ):
        # nouvelle version publiée depuis plus de 24h !
        return f"""<div>Attention: {SCONAME} version ({SCOVERSION}) non à jour
            ({infos["last_version"]} disponible).</div>
            <div>Contacter votre administrateur système
            (<a href="https://scodoc.org/MisesAJour/">documentation</a>).
            </div>
        """
    return ""  # ok
