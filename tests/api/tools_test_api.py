"""Utilitaires pour les tests de l'API
"""
import json


def verify_fields(json_response: dict, expected_fields: set) -> bool:
    """
    Vérifie si les champs attendu de la réponse json sont présents

    json_response : la réponse de la requête
    expected_fields : ensemble des champs à vérifier

    Retourne True ou False
    """
    return all(field in json_response for field in expected_fields)


def verify_occurences_ids_etuds(json_response) -> bool:
    """
    Vérifie si il n'y a pas deux fois le même id dans la liste d'étudiant donnée en paramètres

    json_response : la réponse de la requête

    Retourne True ou False
    """
    etuds = json.loads(json_response)

    ids = [etud["id"] for etud in etuds]
    nips = [etud["code_nip"] for etud in etuds]
    ines = [etud["code_ine"] for etud in etuds]

    return (
        (len(set(ids)) == len(ids))
        and (len(set(nips)) == len(nips))
        and (len(set(ines)) == len(ines))
    )


DEPARTEMENT_FIELDS = [
    "id",
    "acronym",
    "description",
    "visible",
    "date_creation",
]

# Champs "données personnelles"
ETUD_FIELDS_RESTRICTED = {
    "boursier",
}
ETUD_FIELDS = {
    "admission",
    "adresses",
    "civilite",
    "code_ine",
    "code_nip",
    "date_naissance",
    "dept_acronym",
    "dept_id",
    "dept_naissance",
    "id",
    "lieu_naissance",
    "nationalite",
    "nom",
    "prenom",
} | ETUD_FIELDS_RESTRICTED


FORMATION_FIELDS = {
    "dept_id",
    "acronyme",
    "titre_officiel",
    "formation_code",
    "code_specialite",
    "id",
    "titre",
    "version",
    "type_parcours",
    "referentiel_competence_id",
    "formation_id",
}

FORMATION_EXPORT_FIELDS = {
    # "dept_id" "id" "formation_id", pas présents sauf si export_with_ids
    "acronyme",
    "code_specialite",
    "formation_code",
    "referentiel_competence_id",
    "titre_officiel",
    "titre",
    "type_parcours",
    "ue",
    "version",
}

FORMATION_EXPORT_UE_FIELDS = {
    "acronyme",
    "numero",
    "titre",
    "type",
    "ue_code",
    "ects",
    "is_external",
    "code_apogee",
    "coefficient",
    "semestre_idx",
    "color",
    "reference",
    "matiere",
}

FORMATION_EXPORT_UE_MATIERE_FIELDS = {
    "titre",
    "numero",
    "module",
}

FORMATION_EXPORT_UE_MATIERE_MODULE_FIELDS = {
    "titre",
    "abbrev",
    "code",
    "heures_cours",
    "heures_td",
    "coefficient",
    "ects",
    "semestre_id",
    "numero",
    "code_apogee",
    "module_type",
    "coefficients",
}

FORMATION_EXPORT_UE_MATIERE_MODULE_COEF_FIELDS = {
    "ue_reference",
    "coef",
}

FORMSEMESTRE_FIELDS = [
    "titre",
    "gestion_semestrielle",
    "scodoc7_id",
    "date_debut",
    "bul_bgcolor",
    "date_fin",
    "resp_can_edit",
    "dept_id",
    "etat",
    "resp_can_change_ens",
    "id",
    "modalite",
    "ens_can_edit_eval",
    "formation_id",
    "gestion_compensation",
    "elt_sem_apo",
    "semestre_id",
    "bul_hide_xml",
    "elt_annee_apo",
    "block_moyenne_generale",
    "formsemestre_id",
    "titre_num",
    "titre_formation",
    "date_debut_iso",
    "date_fin_iso",
    "responsables",
    "parcours",
    "departement",
    "formation",
    "etape_apo",
    "block_moyennes",
]

FSEM_FIELDS = {
    "block_moyennes",
    "block_moyenne_generale",
    "bul_bgcolor",
    "bul_hide_xml",
    "date_debut_iso",
    "date_debut",
    "date_fin_iso",
    "date_fin",
    "dept_id",
    "elt_annee_apo",
    "elt_sem_apo",
    "ens_can_edit_eval",
    "etat",
    "formation_id",
    "formsemestre_id",
    "gestion_compensation",
    "gestion_semestrielle",
    "id",
    "modalite",
    "resp_can_change_ens",
    "resp_can_edit",
    "responsables",
    "semestre_id",
    "titre_num",
    "titre",
}

MODIMPL_FIELDS = {
    "id",
    "formsemestre_id",
    "computation_expr",
    "module_id",
    "responsable_id",
    "moduleimpl_id",
    "ens",
    "module",
}

MODULE_FIELDS = {
    "heures_tp",
    "code_apogee",
    "titre",
    "coefficient",
    "module_type",
    "id",
    "ects",
    "abbrev",
    "ue_id",
    "code",
    "formation_id",
    "heures_cours",
    "matiere_id",
    "heures_td",
    "semestre_id",
    "numero",
    "module_id",
}

UE_FIELDS = {
    "semestre_idx",
    "type",
    "formation_id",
    "ue_code",
    "id",
    "ects",
    "acronyme",
    "is_external",
    "numero",
    "code_apogee",
    "titre",
    "coefficient",
    "color",
    "ue_id",
}

BULLETIN_FIELDS = {
    "version",
    "type",
    "date",
    "publie",
    "etudiant",
    "formation",
    "formsemestre_id",
    "etat_inscription",
    "options",
    "ressources",
    "saes",
    "ues",
    "semestre",
}


BULLETIN_ETUDIANT_FIELDS = {
    "civilite",
    "code_ine",
    "code_nip",
    "date_naissance",
    "dept_id",
    "dept_acronym",
    "email",
    "emailperso",
    "etudid",
    "nom",
    "prenom",
    "nomprenom",
    "lieu_naissance",
    "dept_naissance",
    "nationalite",
    "boursier",
    "fiche_url",
    "photo_url",
    "id",
    "domicile",
    "villedomicile",
    "telephone",
    "fax",
    "description",
    "codepostaldomicile",
    "paysdomicile",
    "telephonemobile",
    "typeadresse",
}

BULLETIN_FORMATION_FIELDS = {"id", "acronyme", "titre_officiel", "titre"}

BULLETIN_OPTIONS_FIELDS = {
    "show_abs",
    "show_abs_modules",
    "show_ects",
    "show_codemodules",
    "show_matieres",
    "show_rangs",
    "show_ue_rangs",
    "show_mod_rangs",
    "show_moypromo",
    "show_minmax",
    "show_minmax_mod",
    "show_minmax_eval",
    "show_coef",
    "show_ue_cap_details",
    "show_ue_cap_current",
    "show_temporary",
    "temporary_txt",
    "show_uevalid",
    "show_date_inscr",
}

BULLETIN_RESSOURCES_FIELDS = {
    "R101",
    "R102",
    "R103",
    "R104",
    "R105",
    "R106",
    "R107",
    "R108",
    "R109",
    "R110",
    "R111",
    "R112",
    "R113",
    "R114",
    "R115",
}

BULLETIN_SAES_FIELDS = {
    "SAE11",
    "SAE12",
    "SAE13",
    "SAE14",
    "SAE15",
    "SAE16",
}

########### RESSOURCES ET SAES ###########
BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_FIELDS = {
    "id",
    "titre",
    "code_apogee",
    "url",
    "moyenne",
    "evaluations",
}

BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_FIELDS = {
    "id",
    "description",
    "date",
    "heure_debut",
    "heure_fin",
    "coef",
    "poids",
    "note",
    "url",
}

BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_POIDS_FIELDS = {
    "RT1.1",
    "RT2.1",
    "RT3.1",
}

BULLETIN_RESSOURCES_ET_SAES_RESSOURCE_ET_SAE_EVALUATION_NOTE_FIELDS = {
    "value",
    "min",
    "max",
    "moy",
}


########### UES ###########
BULLETIN_UES_FIELDS = {"RT1.1", "RT2.1", "RT3.1"}

BULLETIN_UES_UE_FIELDS = {
    "id",
    "titre",
    "numero",
    "type",
    "color",
    "competence",
    "moyenne",
    "bonus",
    "malus",
    "capitalise",
    "ressources",
    "saes",
    "ECTS",
}

BULLETIN_UES_UE_MOYENNE_FIELDS = {"value", "min", "max", "moy", "rang", "total"}

BULLETIN_UES_RT11_RESSOURCES_FIELDS = {
    "R101",
    "R102",
    "R103",
    "R104",
    "R106",
    "R108",
    "R110",
    "R111",
    "R112",
    "R113",
    "R114",
}

BULLETIN_UES_RT21_RESSOURCES_FIELDS = {
    "R101",
    "R103",
    "R104",
    "R105",
    "R110",
    "R111",
    "R112",
    "R113",
    "R114",
    "R115",
}

BULLETIN_UES_RT31_RESSOURCES_FIELDS = {
    "R101",
    "R107",
    "R108",
    "R109",
    "R110",
    "R111",
    "R112",
    "R115",
}

BULLETIN_UES_UE_RESSOURCES_RESSOURCE_FIELDS = {"id", "coef", "moyenne"}

BULLETIN_UES_RT11_SAES_FIELDS = {
    "SAE11",
    "SAE12",
}

BULLETIN_UES_RT21_SAES_FIELDS = {"SAE13"}

BULLETIN_UES_RT31_SAES_FIELDS = {
    "SAE14",
    "SAE15",
}

BULLETIN_UES_UE_SAES_SAE_FIELDS = {"id", "coef", "moyenne"}


BULLETIN_UES_UE_ECTS_FIELDS = {"acquis", "total"}


########### SEMESTRE ###########
BULLETIN_SEMESTRE_FIELDS = {
    "etapes",
    "date_debut",
    "date_fin",
    "annee_universitaire",
    "numero",
    "inscription",
    "groupes",
    "absences",
    "ECTS",
    "notes",
    "rang",
}

BULLETIN_SEMESTRE_ABSENCES_FIELDS = {"injustifie", "total"}

BULLETIN_SEMESTRE_ECTS_FIELDS = {"acquis", "total"}

BULLETIN_SEMESTRE_NOTES_FIELDS = {"value", "min", "moy", "max"}

BULLETIN_SEMESTRE_RANG_FIELDS = {"value", "total"}


EVAL_FIELDS = {
    "id",
    "description",
    "date_debut",
    "date_fin",
    "coefficient",
    "evaluation_type",
    "moduleimpl_id",
    "note_max",
    "numero",
    "poids",
    "publish_incomplete",
    "visibulletin",
    "etat",
    "nb_inscrits",
    "nb_notes_manquantes",
    "nb_notes_abs",
    "nb_notes_att",
    "nb_notes_exc",
    "saisie_notes",
}

SAISIE_NOTES_FIELDS = {"datetime_debut", "datetime_fin", "datetime_mediane"}

REF_COMP_FIELDS = {
    "dept_id",
    "annexe",
    "specialite",
    "specialite_long",
    "type_structure",
    "type_departement",
    "type_titre",
    "version_orebut",
    "scodoc_date_loaded",
    "scodoc_orig_filename",
    "competences",
    "parcours",
}

ABSENCES_FIELDS = {
    "jour",
    "matin",
    "estabs",
    "estjust",
    "description",
    "begin",
    "end",
}

ABSENCES_GROUP_ETAT_FIELDS = {"etudid", "list_abs"}


FORMSEMESTRE_ETUD_FIELDS = {
    "id",
    "code_nip",
    "code_ine",
    "nom",
    "nom_usuel",
    "prenom",
    "civilite",
    "groups",
}

FORMSEMESTRE_ETUS_GROUPS_FIELDS = {
    "partition_id",
    "id",
    "formsemestre_id",
    "partition_name",
    "numero",
    "bul_show_rank",
    "show_in_lists",
    "group_id",
    "group_name",
}

EVALUATIONS_FIELDS = {
    "coefficient",
    "date_debut",
    "date_fin",
    "description",
    "evaluation_type",
    "id",
    "note_max",
    "numero",
    "poids",
    "publish_incomplete",
    "visibulletin",
}

NOTES_FIELDS = {
    "etudid",
    "evaluation_id",
    "value",
    "comment",
    "date",
    "uid",
}


PARTITIONS_FIELDS = {
    "id",
    "formsemestre_id",
    "partition_name",
    "numero",
    "bul_show_rank",
    "show_in_lists",
}

PARTITION_GROUPS_ETUD_FIELDS = {
    "id",
    "dept_id",
    "nom",
    "prenom",
    "nom_usuel",
    "civilite",
    "code_nip",
    "code_ine",
}

FORMSEMESTRE_BULLETINS_FIELDS = {
    "version",
    "type",
    "date",
    "publie",
    "etudiant",
    "formation",
    "formsemestre_id",
    "etat_inscription",
    "options",
    "ressources",
    "saes",
    "ues",
    "semestre",
}

FORMSEMESTRE_BULLETINS_ETU_FIELDS = {
    "civilite",
    "code_ine",
    "code_nip",
    "date_naissance",
    "dept_id",
    "dept_acronym",
    "email",
    "emailperso",
    "etudid",
    "nom",
    "prenom",
    "nomprenom",
    "lieu_naissance",
    "dept_naissance",
    "nationalite",
    "boursier",
    "fiche_url",
    "photo_url",
    "id",
    "codepostaldomicile",
    "paysdomicile",
    "telephonemobile",
    "typeadresse",
    "domicile",
    "villedomicile",
    "telephone",
    "fax",
    "description",
}

FORMSEMESTRE_BULLETINS_FORMATION_FIELDS = {
    "id",
    "acronyme",
    "titre_officiel",
    "titre",
}

FORMSEMESTRE_BULLETINS_OPT_FIELDS = {
    "show_abs",
    "show_abs_modules",
    "show_ects",
    "show_codemodules",
    "show_matieres",
    "show_rangs",
    "show_ue_rangs",
    "show_mod_rangs",
    "show_moypromo",
    "show_minmax",
    "show_minmax_mod",
    "show_minmax_eval",
    "show_coef",
    "show_ue_cap_details",
    "show_ue_cap_current",
    "show_temporary",
    "temporary_txt",
    "show_uevalid",
    "show_date_inscr",
}
