"""Test formsemestre

Utilisation :
    créer les variables d'environnement: (indiquer les valeurs
    pour le serveur ScoDoc que vous voulez interroger)

    export SCODOC_URL="https://scodoc.xxx.net/"
    export SCODOC_USER="xxx"
    export SCODOC_PASSWD="xxx"
    export CHECK_CERTIFICATE=0 # ou 1 si serveur de production avec certif SSL valide

    (on peut aussi placer ces valeurs dans un fichier .env du répertoire tests/api).

    Lancer :
        pytest tests/api/test_api_formsemestre.py
"""
import requests

from app.scodoc import sco_utils as scu

from tests.api.setup_test_api import (
    API_URL,
    CHECK_CERTIFICATE,
    api_headers,
)


def test_save_groups_auto_assignment(api_headers):
    """
    Routes:
        /formsemestre/<id>/save_groups_auto_assignment
        /formsemestre/<id>/get_groups_auto_assignment
    """
    formsemestre_id = 1
    r = requests.get(
        f"{API_URL}/formsemestre/{formsemestre_id}",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    # On stocke une chaine quelconque
    data_orig = (
        """{ "attribute" : "Un paquet de json", "valide": pas nécessairement +}--"""
    )
    r = requests.post(
        f"{API_URL}/formsemestre/{formsemestre_id}/save_groups_auto_assignment",
        data=data_orig.encode("utf-8"),
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    # GET
    r = requests.get(
        f"{API_URL}/formsemestre/{formsemestre_id}/get_groups_auto_assignment",
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 200
    assert r.text == data_orig
    # Tente d'envoyer trop de données
    r = requests.post(
        f"{API_URL}/formsemestre/{formsemestre_id}/save_groups_auto_assignment",
        data="F*CK" * 1000000,  # environ 4MB
        headers=api_headers,
        verify=CHECK_CERTIFICATE,
        timeout=scu.SCO_TEST_API_TIMEOUT,
    )
    assert r.status_code == 413
